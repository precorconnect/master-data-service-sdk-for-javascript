/**
 * @class {PartnerSaleSimpleLineItemWebDto}
 */
export default class PartnerSaleSimpleLineItemWebDto {

    _serialNumber:string;


    /**
     * @param serialNumber
     */
    constructor(
        serialNumber:string
    ) {
        this._serialNumber = serialNumber;
    }

    get serialNumber():string {
        return this._serialNumber;
    }

    toJSON() {
        return {
            serialNumber:this._serialNumber
        }
    }
}
