import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import ProductSaleRegistrationRequestWebDto from './ProductSaleRegistrationRequestWebDto';
import ProductSaleRegistrationResponseWebDto from './ProductSaleRegistrationResponseWebDto';

/**
 * class SpiffAmountSerialNumberFeature
 */
@inject(SpiffMasterDataServiceSdkConfig, HttpClient)
class SpiffAmountSerialNumberFeature {

    _config:SpiffMasterDataServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }


    /**
     * @param {ProductSaleRegistrationRequestWebDto} request
     * @param {string} accessToken
     * @returns {Promise.<T>|Promise}
     */
    execute(
        request:ProductSaleRegistrationRequestWebDto,
        accessToken:string
        ):Promise<ProductSaleRegistrationResponseWebDto> {

        return this._httpClient
            .createRequest('spiff-master-data/spiffAmountSerialNumber')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response =>
                    response.content
                )
    }

}
export default SpiffAmountSerialNumberFeature;