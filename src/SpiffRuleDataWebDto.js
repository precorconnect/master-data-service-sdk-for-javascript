/**
 * @class{SpiffRuleDataWebDto}
 */

export  default class SpiffRuleDataWebDto {

    _id:number;

    _spiffId:number;

    _serialNumber:string;

    _description:string;

    _multipleRequired:string;


    /**
     *  @param {number} id
     * @param {number} spiffId
     * @param {string} serialNumber
     * @param {string} description
     * @param {string} multipleRequired
     *
     */

    constructor(id:number,
                spiffId:number,
                serialNumber:string,
                description:string,
                multipleRequired:string) {

        if (!id) {
            throw new TypeError('spiffRuleId required');
        }
        this._id = id;

        if (!spiffId) {
            throw new TypeError('spiffId required');
        }
        this._spiffId = spiffId;

        if (!serialNumber) {
            throw  new TypeError('serialNumber required');
        }
        this._serialNumber = serialNumber;

        if (!description) {
            throw new TypeError('description required');
        }
        this._description = description;

        if (!multipleRequired) {
            throw  new TypeError('multipleRequired required');
        }
        this._multipleRequired = multipleRequired;

    }


    /**
     * @returns {number}
     */
    get spiffId():number{
        return this._spiffId;
    }

    /**
     * @returns {string}
     */
    get serialNumber():string{
        return this._serialNumber;
    }

    /**
     * @returns {string}
     */
    get description():string{
        return this._description;
    }

    /**
     * @returns {string}
     */
    get multipleRequired():string{
        return this._multipleRequired;
    }

    toJSON(){
        return {
            id:this._id,
            spiffId:this._spiffId,
            serialNumber:this._serialNumber,
            description:this._description,
            multipleRequired:this._multipleRequired
        };
    }

}
