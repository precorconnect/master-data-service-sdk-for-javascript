import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import SaveMasterDataFeature from './SaveMasterDataFeature';
import SaveRuleDataFeature from './SaveRuleDataFeature';
import UpdateMasterDataFeature from './UpdateMasterDataFeature'
import UpdateSpiffRuleDataFeature from './UpdateSpiffRuleDataFeature';
import SpiffAmountSerialNumberFeature from './SpiffAmountSerialNumberFeature';

/**
 *@class {DiContainer}
 */
export  default class  DiContainer{

    _container: Container;

    /**
     * @param{SpiffMasterDataServiceSdkConfig} config
     */

    constructor(config:SpiffMasterDataServiceSdkConfig){

        if(!config){
            throw 'config required';
        }
        this._container=new Container();

        this._container.registerInstance(SpiffMasterDataServiceSdkConfig,config);
        this._container.autoRegister(HttpClient);
        this.__registerFeatures();
    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    __registerFeatures(){
        this._container.autoRegister(SaveMasterDataFeature);
        this._container.autoRegister(SaveRuleDataFeature);
        this._container.autoRegister(UpdateMasterDataFeature);
        this._container.autoRegister(UpdateSpiffRuleDataFeature);
        this._container.autoRegister(SpiffAmountSerialNumberFeature);
    }
}