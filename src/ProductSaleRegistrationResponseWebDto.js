import SimpleLineItemWithPriceWebDto from './SimpleLineItemWithPriceWebDto';
import CompositeLineItemWithPriceWebDto from './CompositeLineItemWithPriceWebDto';
/**
 * @class{ProductSaleRegistrationResponseWebDto}
 */

export  default class ProductSaleRegistrationResponseWebDto {

    _simpleSerialCode:SimpleLineItemWithPriceWebDto[];

    _compositeSerialCode:CompositeLineItemWithPriceWebDto[];

    constructor(
        simpleSerialCode:SimpleLineItemWithPriceWebDto[],
        compositeSerialCode:CompositeLineItemWithPriceWebDto[]
    ) {

        this._simpleSerialCode = simpleSerialCode;

        this._compositeSerialCode = compositeSerialCode;
    }

    get simpleSerialCode():SimpleLineItemWithPriceWebDto[] {
        return this._simpleSerialCode;
    }

    get compositeSerialCode():CompositeLineItemWithPriceWebDto[] {
        return this._compositeSerialCode;
    }

    toJSON(){
        return {
            simpleSerialCode: this._simpleSerialCode,
            compositeSerialCode: this._compositeSerialCode
        }
    }

}
