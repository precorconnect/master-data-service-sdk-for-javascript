import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import  SpiffMasterDataWebView from './SpiffMasterDataWebView';

@inject(SpiffMasterDataServiceSdkConfig,HttpClient)
class ListMasterDataFeature{

    _config:SpiffMasterDataServiceSdkConfig;
    _httpClient:HttpClient;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient){
        if(!config){
            throw 'config required';
        }
        this._config=config;

        if(!httpClient){
            throw 'httpClient required';
        }
        this._httpClient=httpClient;
    }

    /**
     * Lists SpiffMasterData
     * @param {string} accessToken
     * @returns {Promise.<SpiffMasterDataWebView[]>}
     */

    execute(accessToken:string):Promise<SpiffMasterDataWebView[]> {

        return this._httpClient
            .createRequest(`spiff-master-data/masterdata`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                return Array.from(
                        response.content,
                        contentItem =>
                            new SpiffMasterDataWebView(
                                contentItem.id,
                                contentItem.name,
                                contentItem.brand,
                                contentItem.startDate,
                                contentItem.endDate,
                                contentItem.amount,
                                contentItem.graceperiod,
                                contentItem.status
                            )
                    );

            });
    }
}


export  default ListMasterDataFeature;